import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
// 公用请求方法
import { basePost, baseGet, tokenPost, tokenGet } from '@/api/request.js'
Vue.prototype.basePost = basePost
Vue.prototype.baseGet = baseGet
Vue.prototype.tokenPost = tokenPost
Vue.prototype.tokenGet = tokenGet
// 接口对象，所有请求接口均挂在此对象上
import api from '@/api/index.js'
Vue.prototype.api = api

import {indexMixin} from '@/mixin/index.js'
Vue.mixin(indexMixin);

App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif