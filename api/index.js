// 生产环境
const proUrl = 'http://pay.baispay.cn/'
// 开发环境-远程穿透
const devUrl = 'http://139.129.27.88:8201/' // 穿透测试服务器域名
const mockDev = 'http://192.168.3.7:3000/' // 本地mock数据的服务端局域网IP地址，需启动mock项目，并手动修改本ip

// encode编码后的域名
const secretPrePro = 'https%3a%2f%2fshopping.odinjilin.com%2f%23%2f'
const secretPreDev = 'http%3a%2f%2fjilinodin.nat100.top%2f%23%2f'

let urlPre, secretPreUrl
// 环境配置
if (process.env.NODE_ENV === 'development') {
	// 开发环境
	// console.log('开发环境',process.env.NODE_ENV)
	urlPre = proUrl
	secretPreUrl = secretPreDev
} else if (process.env.NODE_ENV === 'production') {
	// 生产环境
	// console.log('生产环境',process.env)
	urlPre = proUrl
	secretPreUrl = secretPrePro
} else {
	console.log('未知环境', process.env)
}


// 微信登录，并重定向到第三方网站的加密地址，重定向到了“我的”页面
const toPersonSecretUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx26a5b69be5b4dad3&redirect_uri=' +
	secretPreUrl +
	'pages%2ftabBar%2fpersonalCenter%2fpersonalCenter&response_type=code&scope=snsapi_userinfo&state=123&connect_redirect=1#wechat_redirect'
// 重定向到了“商品”页面
const toCommInfoSecretUrl =
	'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx26a5b69be5b4dad3&redirect_uri=' + secretPreUrl +
	'pages%2fcommInfo%2fcommInfo&response_type=code&scope=snsapi_userinfo&state=123&connect_redirect=1#wechat_redirect'
// const toLoginSecretUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx26a5b69be5b4dad3&redirect_uri='+ secretPreUrl +'pages%login%2flogin&response_type=code&scope=snsapi_userinfo&state=123&connect_redirect=1#wechat_redirect'

let api = {
	mockDev,
	// 域名
	urlPre: urlPre,
	SecretUrl: toPersonSecretUrl,
	ToCommInfoSecretUrl: toCommInfoSecretUrl,

	// 通过channelCod获取信息
	GetFixedCode: 'api/fixedCode/',
	ToPay: 'api/fixedCode/toPay',


	// 报修申报
	GetRegionList: 'api/region/',
	GetRegisterDeptList: 'api/register/dept/list',
	GetRegisterJobList: 'api/register/job/list/',
	Register: 'api/register',
	Login: "api/openid/login",
	RepairAdd: 'api/repair/add',
	RepairList: 'api/repair/list/',
	RepairprocessList: 'api/repair/process/',
	GetRepairInfo: 'api/repair/info/',
	GetDeptWsList: 'api/dept/ws/list',
	GetUserSfList: 'api/user/sf/list/',
	GetProblemTopList: 'api/problem/top/list/',
	GetProblemList: 'api/problem/list/',
	RepairAllocation: 'api/repair/allocation',
	RepairReject: 'api/repair/reject',
	RepairAccept: 'api/repair/accept',
	RepairComplete: 'api/repair/complete',
	RepairExamine: 'api/repair/examine',
	RepairEvaluate: 'api/repair/evaluate',
	GetHiddenDangerList: 'api/hiddenDanger/list/',
	HiddenDangerAdd: 'api/hiddenDanger/add',
	HiddenDangerAllocation: 'api/hiddenDanger/allocation',
	HiddenDangerComplete: 'api/hiddenDanger/complete',
	HiddenDangerExamine: 'api/hiddenDanger/examine',
	GetApplyList: 'api/apply/list/',
	ApplySecurity: 'api/apply',
	ApplyExamine: 'api/apply/examine',
	UserCommute: 'api/user/commute/',
	GetPatrolLogNum: 'api/patrolLog/num/',
	PatrolLogAdd: 'api/patrolLog/add',
	GetPatrolLogList: 'api/patrolLog/list/',
	Upload: "api/api/upload",
	CodeLogin: 'api/wechat/auth', // code登录
	// 微信相关
	// POST：获取微信支付sdk签名信息
	WxPrePayUrl: 'txqMall/wx/wxPrePay',
	// GET：获取用户“微信”信息（头像、openid、nickname）
	WxUserInfoUrl: 'txqMall/wx/wxUserInfo',
	// GET：获取微信sdk签名信息
	GetWxConfigUrl: 'txqMall/wx/getWxConfig',

	// 用户相关
	// POST：用户是否注册过、邀请码是否正确
	SmsLogonUrl: 'txqMall/register/smsLogon',
	// POST：校验验证码
	CheckMsgCodeUrl: 'txqMall/register/checkMsgCode',
	// POST：用户注册
	LogonUrl: 'txqMall/register/logon',
	// POST：修改用户信息,userId\userName
	UpdateUserNameUrl: 'txqMall/user/updateUserName',
	// POST：获取用户“商城”信息
	GetUserInfoUrl: 'txqMall/user/getUserInfo',
	// POST：获取用户"冻结金额"\"推广人数"\"可提现金额"，入参userId
	GetMoneyUrl: 'txqMall/user/getMoney',
	// POST：用户钱包金额\提现中金额
	GetBalanceSummaryUrl: 'txqMall/user/getBalanceSummary',
	// POST：用户钱包明细
	GetBalanceDetailUrl: 'txqMall/user/getBalanceDetail',
	// POST：提现前，获取数据库中，用户是否已申请提现过，提现过则返回姓名、手机号
	GetWithdrawalUserInfoUrl: 'txqMall/user/getWithdrawalUserInfo',
	// POST：用户提现
	AddUseWithdrawUrl: 'txqMall/user/addUseWithdraw',
	// 获取下线，推广人列表
	GetChildListUrl: 'txqMall/user/getChildList',

	// 商品相关
	// GET：根据commCode获取CommId
	GetCommIdByCommCodeUrl: 'txqMall/commodity/getCommIdByCommCode',
	// POST：获取商品列表(入参无commName), 搜索商品(入参有commName)
	GetByUserSlightUrl: 'txqMall/commodity/getByUserSlight',
	// POST：获取分享海报图片的base64
	CommShareUrl: 'txqMall/user/commShare',
	// POST：获取商品详情
	GetDetailUrl: 'txqMall/commodity/getDetail',
	// POST：获取商品轮播图
	GetPhotosUrl: 'txqMall/photo/getPhotos',

	// 订单相关（payState--1：已支付，0：未支付   2 ：退款中 -1：已退款   ）
	// POST：添加订单
	AddOrderDtlUrl: 'txqMall/order/addOrderDtl',
	// POST：获取订单列表
	GetOrderListUrl: 'txqMall/order/getOrderList',
	// POST：删除订单
	DeleteUnpaidOrderUrl: 'txqMall/order/deleteUnpaidOrder',
	// POST：获取订单详情（正规途径获取，即商城的订单列表页）
	GetOrderInfoUrl: 'txqMall/order/getOrderInfo',
	// POST：获取订单详情（短信链接获取）
	GetOrderInfoBysmsCodeUrl: 'txqMall/order/getOrderInfoBysmsCode',
	// 订单退款
	UserCashOutUrl: 'txqMall/order/userCashOut',

	// 核销相关
	// POST: 获取核销页面数据
	VerificationUrl: 'txqMall/shop/verification',
	//POST: 确认核销（手机端废弃不用）
	// ModifyStateOrderdtlUrl:'txqMall/order/modifyStateOrderdtl',
	// POST：核销订单
	VerificationOrderdtlUrl: 'txqMall/order/verificationOrderdtl',

	// 商户中心相关
	// POST：获取商户"交易笔数、余额、提现中"，入参：shopCode
	GetShopBalanceInfoUrl: 'txqMall/shop/getShopBalanceInfo',
	// POST：商户提现，shopCode\operCode（userId）\operName(userName)\cost 提现金额
	AddShopWithdrawUrl: 'txqMall/shop/addShopWithdraw',
	// POST：获取商户交易明细信息，shopCode\date 查询日期（20201127，未选择时间则为当前时间）
	GetShopBalanceDetailUrl: 'txqMall/shop/getShopBalanceDetail',
	// POST：获取商户结算信息，入参date为空，查询总结算信息，有值（如20201215）则查询传入的那天结算信息
	GetShopDayBalanceUrl: 'txqMall/shop/getShopDayBalance'
}

export default api
