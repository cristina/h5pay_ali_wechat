import api from './index.js'

// 域名，在api/index.js文件中配置
let _baseUrl = api.urlPre

// 手机端弹出错误信息
let _showToast = (msg) => {
	uni.showToast({
		title: msg,
		icon: 'none',
		mask: true,
		duration: 2000
	})
}

// 响应拦截，进行错误统一处理
let _errHandle = (fromFuncName, opts, err) => {
	uni.hideLoading()
	console.log(err, "响应拦截");
	let status = err.statusCode ? err.statusCode : err.status;
	let errObj = {
		'错误码': status ? status : '无',
		'来源': fromFuncName,
		'报错信息': err
	}
	// let msg ='错误码：' + err.statusCode + '\r\n' + '来源：' + fromFuncName + '\r\n' + '报错信息：' + JSON.stringify(err.data)
	// 手机端需弹出的信息
	if (status == 401) {
		// 静默获取openid，重新执行登录操作
		loginUser();
	} else if (status >= 500) {
		_showToast('服务器错误')
	} else if (!status) {
		if (err.errMsg === 'request:fail') {
			_showToast('服务器可能挂了')
		} else {
			_showToast(err.errMsg)
		}
	}
	// 开发者工具需打印的信息
	console.log('错误：', errObj, opts)
}
async function loginUser() {
	// 测试数据，暂时写死
	let param = {
		"openid": uni.getStorageSync('token')
	};
	let resLogin = await basePost(api.Login, param);
	if (resLogin.status == 200) {
		let info = resLogin.data;
		// 此处存储数据，跳到所在页
		//  "is_register": false, //是否需要跳到注册  false 不需要
		uni.setStorageSync("workStatus", info.workStatus); //工作状态 0上班 1下班
		uni.setStorageSync("isTreacherousChiefs", info
			.isTreacherousChiefs); // 是否是安全隐患负责人 0否 1是  //显示安全隐患上报按钮
		uni.setStorageSync("isSafetyChiefs", info
			.isSafetyChiefs); // 是否是安全主管（审核安全员申请）0否 1是  //显示安全隐患上报按钮
		uni.setStorageSync("isSafetyGuards", info
			.isSafetyGuards); //是否是安全员 0否 1是  //显示安全隐患上报按钮
		uni.setStorageSync("nickName", info.nickName);
		uni.setStorageSync("phone", info.phone);
		uni.setStorageSync("job", info.job);
		uni.setStorageSync("deptName", info.deptName);
		uni.setStorageSync("deptPhone", info.deptPhone); //科室电话
		uni.setStorageSync("deptAddr", info.deptAddr); //科室地址
		uni.setStorageSync("token", 'Bearer '+info.token);
		const currentPages = getCurrentPages();
		uni.navigateTo({
			url: `/${currentPages[currentPages.length - 1].route}`
		});
	}
}
// 封装uni.request的Promise
let _baseRequest = (opts) => {
	// console.log(_baseUrl + opts.url)
	return new Promise((resolve, reject) => {
		uni.request({
			url: _baseUrl + opts.url,
			// url: opts.url,
			header: opts.header,
			data: opts.data,
			method: opts.method,
			success: (res) => {
				// console.log('request.js',res)
				if (res.statusCode >= 200 && res.statusCode < 300) {
					// console.log('request.js',res)
					if (res.data.status != 401) {
						resolve(res.data)
					} else {
						// 错误统一处理
						_errHandle('success方法', opts, res.data)
					}
				} else {
					// 错误统一处理
					_errHandle('success方法', opts, res)
				}
			},
			fail: (err) => {
				// 错误统一处理
				_errHandle('fail方法', opts, err)
				reject(err)
			}
		})
	}).catch((e) => {})
}

// 以上定义，以下导出

// main.js中挂载到Vue原型上,可在vue文件内通过 this.xxx(如this.basePost) 使用，使用时不需catch错误，错误会在_errHandle方法中统一弹出和打印
// 公用post请求
export function basePost(url, param) {
	let opts = {
		'url': url,
		'data': param,
		'method': 'POST',
		'header': {
			'content-type': 'application/json'
		}
	}
	return _baseRequest(opts)
}
// 公用get请求
export function baseGet(url, param, isLine) {
	let urls = isLine ? url + param : url;
	let opts = {
		'url': urls,
		'data': isLine ? undefined : param,
		'method': 'GET',
		'header': {}
	}
	return _baseRequest(opts)
}
let tokenBase = (url, param, type, isLine) => {
	let token = uni.getStorageSync('token');
	if (!token) {
		uni.showToast({
			title: '无登录凭证，请重新登录',
			icon: 'none',
			duration: 2000,
			mask: true
		})
		setTimeout(() => {
			uni.navigateTo({
				url: '/pages/login/login'
			})
		}, 2000)
		return
	}
	let urls = isLine ? url + param : url;
	let opts = {
		'url': urls,
		'data': isLine ? undefined : param,
		'method': type,
		'header': {
			'content-type': 'application/json',
			'Authorization': token,
		}
	}
	return _baseRequest(opts)
}
// 携带token验证登录权限的公用请求
// export function tokenPost(url,param){
// 	param = param ? param : null
// 	let token = uni.getStorageSync('token')
// 	if(!token){
// 		uni.showToast({
// 			title:'无登录凭证，请重新登录',
// 			icon:'none',
// 			duration:2000,
// 			mask:true
// 		})
// 		setTimeout(()=>{
// 			uni.switchTab({
// 				url:'/pages/doctor/doctor'
// 			})
// 		},2000)
// 		return
// 	}
// 	let opts = {
// 			'url' : url,
// 			'data' : param,
// 			'method' : 'POST',
// 			'header' : {
// 				'content-type': 'application/json',
// 				'token':token,
// 			}
// 		}
// 	return _baseRequest(opts)
// }

export function tokenPost(url, param, isLine) {
	return tokenBase(url, param, 'POST', isLine)
}
export function tokenGet(url, param, isLine) {
	return tokenBase(url, param, 'GET', isLine)
}
