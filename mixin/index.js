export const indexMixin = {
	mounted() {
		if (this.isWeiXinBrowser() || this.isQQBrowser()) {
			this.navTitle()
		}
	},
	methods: {
		isWeiXinBrowser() { //判断是否为微信
			let ua = navigator.userAgent.toLowerCase()
			return ua.indexOf('micromessenger') != -1
		},
		isQQBrowser() { //判断是否为qq
			var ua = navigator.userAgent.toLowerCase()
			if (ua.match(/QQ/i) == "qq") {
				return true
			} else {
				return false
			}
		},
		navTitle() {
			this.$nextTick(() => {
				let navTitleDom = document.getElementsByTagName('uni-page-head')
				if (navTitleDom.length) {
					navTitleDom[0].style.display = 'none'
				}
			})
		},
	}
}
